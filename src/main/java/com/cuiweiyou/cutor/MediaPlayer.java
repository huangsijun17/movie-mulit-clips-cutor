package com.cuiweiyou.cutor;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.concurrent.Semaphore;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import org.bytedeco.javacpp.avutil;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;

/**
 * 和swing界面绑定的播放器
 * <p>
 * 参考：
 * https://blog.csdn.net/Felix_Dreammaker/article/details/79180394
 * https://blog.csdn.net/a694543965/article/details/78387156
 * http://blog.csdn.net/A694543965/article/details/78317479
 * <p>
 * www.gaohaiyan.com
 */
public class MediaPlayer extends Thread {
    private int volume = 1;
    private boolean isPlay;
    private boolean isStop;
    private int sampleFormat;
    private PlayView player;
    private String mediaPath;
    private SourceDataLine sourceDataLine;
    private FFmpegFrameGrabber fmpegFrameGrabber;
    private Semaphore semaphore = new Semaphore(0); //视频播放控制锁
    private OnMoviePlayingListener onMoviePlayingListener;
    private long duration;
    private int lengthInFrames;
    private int newFrameProgress = -1;

    /**
     * 播放器
     *
     * @param cp       播放控件
     * @param path     视频文件路径
     * @param listener 播放回调
     */
    public MediaPlayer(PlayView cp, String path, OnMoviePlayingListener listener) {
        player = cp;
        mediaPath = path;
        isPlay = true;
        isStop = false;
        onMoviePlayingListener = listener;
    }

    /**
     * 设置音量
     *
     * @param vol 默认原音量1；趋于0变小，趋于2变大。0 -> 1 <- 2
     */
    public void setVolume(int vol) {
        this.volume = vol;
    }

    /**
     * 继续播放视频
     */
    public void play() {
        try {
            if (isStop) { // 未播放、播放完毕、手动终止
                fmpegFrameGrabber.restart(); // 重新播放
                sourceDataLine.start();
                isStop = false;

                if (null != onMoviePlayingListener) {
                    onMoviePlayingListener.onMovieDuration(duration, lengthInFrames);
                }
            }

            isPlay = true;
            semaphore.release();
        } catch (FrameGrabber.Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 暂停播放
     */
    public void pause() {
        isPlay = false;
    }

    /**
     * 停止播放
     */
    public void stopPlay() {
        isPlay = false;
        isStop = true;

        if (null != fmpegFrameGrabber) {
            try {
                sourceDataLine.stop();
                sourceDataLine.flush();
                fmpegFrameGrabber.stop();
            } catch (FrameGrabber.Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 设置新的播放进度
     *
     * @param progress
     */
    public synchronized void setProgress(int progress) {
        if (progress > lengthInFrames) {
            progress = lengthInFrames - 10;
        }
        newFrameProgress = progress;
    }

    /**
     * 是否播放状态
     *
     * @return true-播放中；false-暂停中
     */
    public boolean isPlay() {
        return isPlay;
    }

    @Override
    public void run() {

        createPlayer();

        try {
            while (true) {
                if (!isPlay || isStop) {  // 暂停、停止，此时阻塞播放器线程
                    semaphore.acquire();
                }

                if (-1 != newFrameProgress) {  // 手动调整播放位置
                    fmpegFrameGrabber.setFrameNumber(newFrameProgress);
                    newFrameProgress = -1;
                }

                Frame frame = fmpegFrameGrabber.grab(); // 视频+音频合一的帧
                if (null == frame) { // 播放完毕
                    stopPlay();
                    if (null != onMoviePlayingListener) {
                        onMoviePlayingListener.onMoviePlayFinish();
                    }
                    semaphore.acquire();
                    continue;
                }

                int frameNumber = fmpegFrameGrabber.getFrameNumber(); // 正在播放的视频的帧索引。用于更新播放进度条
                if (null != onMoviePlayingListener) {
                    onMoviePlayingListener.onMoviePlaying(frame.timestamp, frameNumber);
                }

                this.player.playFrame(frame);                   // 播放视频
                playAudio(sampleFormat, frame, sourceDataLine); // 播放音频
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 播放帧
     *
     * @param sampleFormat   帧频帧率。44100
     * @param audioFrame     音频帧
     * @param sourceDataLine 声音管道
     */
    private void playAudio(int sampleFormat, Frame audioFrame, SourceDataLine sourceDataLine) {
        Buffer[] buf = audioFrame.samples;
        if (null == buf)
            return;

        switch (sampleFormat) {
            case avutil.AV_SAMPLE_FMT_S16P://平面型左右声道分开
                ShortBuffer leftS16pData = (ShortBuffer) buf[0];
                ShortBuffer rightS16pData = (ShortBuffer) buf[1];
                ByteBuffer leftS16pBytesData = shortToByteValue(leftS16pData, volume);
                ByteBuffer rightS16pBytesData = shortToByteValue(rightS16pData, volume);
                byte[] combineChannelBytes = combineChannelBytes(leftS16pBytesData, rightS16pBytesData);
                sourceDataLine.write(combineChannelBytes, 0, combineChannelBytes.length);
                break;
            case avutil.AV_SAMPLE_FMT_FLTP://平面型左右声道分开。
                FloatBuffer leftFltpData = (FloatBuffer) buf[0];
                FloatBuffer rightFltpData = (FloatBuffer) buf[1];
                ByteBuffer leftFltpBytesData = floatToByteValue(leftFltpData, volume);
                ByteBuffer rightFltpBytesData = floatToByteValue(rightFltpData, volume);
                byte[] combineBytes = combineChannelBytes(leftFltpBytesData, rightFltpBytesData);
                sourceDataLine.write(combineBytes, 0, combineBytes.length);
                break;
            case avutil.AV_SAMPLE_FMT_S16://非平面型左右声道在一个buffer中。
                ShortBuffer s16Data = (ShortBuffer) buf[0];
                ByteBuffer s16BytesData = shortToByteValue(s16Data, volume);
                byte[] channelsBytes = s16BytesData.array();
                sourceDataLine.write(channelsBytes, 0, channelsBytes.length);
                break;
            case avutil.AV_SAMPLE_FMT_FLT://float非平面型
                FloatBuffer fltData = (FloatBuffer) buf[0];
                ByteBuffer fltBytesData = floatToByteValue(fltData, volume);
                byte[] fltBytes = fltBytesData.array();
                sourceDataLine.write(fltBytes, 0, fltBytes.length);
                break;
            default:
                System.err.println("不支持的音频格式");
                break;
        }
    }

    private byte[] combineChannelBytes(ByteBuffer leftBuf, ByteBuffer rightBuf) {
        byte[] leftBytes = leftBuf.array();
        byte[] rightBytes = rightBuf.array();
        byte[] combine = new byte[leftBytes.length + rightBytes.length];
        int step = 0;
        for (int i = 0; i < leftBytes.length; i = i + 2) {
            for (int j = 0; j < 2; j++) {
                combine[j + 4 * step] = leftBytes[i + j];
                combine[j + 2 + 4 * step] = rightBytes[i + j];
            }
            step++;
        }

        return combine;
    }

    private SourceDataLine getSourceDataLine(FFmpegFrameGrabber frameGrabber) {
        AudioFormat.Encoding encoding = AudioFormat.Encoding.PCM_SIGNED;
        int audioChannels = frameGrabber.getAudioChannels();
        int sampleRate = frameGrabber.getSampleRate();
        int frameSize = audioChannels * 2;
        int sampleSizeInBits_16 = 16;
        int sampleSizeInBits_32 = 32;
        boolean bigEndian = true;
        AudioFormat audioFormat = null;

        switch (frameGrabber.getSampleFormat()) {
            case avutil.AV_SAMPLE_FMT_U8://无符号short 8bit
                break;
            case avutil.AV_SAMPLE_FMT_S16://有符号short 16bit
                audioFormat = new AudioFormat(encoding, sampleRate, sampleSizeInBits_16, audioChannels, frameSize, sampleRate, bigEndian);
                break;
            case avutil.AV_SAMPLE_FMT_S32:
                break;
            case avutil.AV_SAMPLE_FMT_FLT:
                audioFormat = new AudioFormat(encoding, sampleRate, sampleSizeInBits_16, audioChannels, frameSize, sampleRate, bigEndian);
                break;
            case avutil.AV_SAMPLE_FMT_DBL:
                break;
            case avutil.AV_SAMPLE_FMT_U8P:
                break;
            case avutil.AV_SAMPLE_FMT_S16P://有符号short 16bit,平面型
                audioFormat = new AudioFormat(encoding, sampleRate, sampleSizeInBits_16, audioChannels, frameSize, sampleRate, bigEndian);
                break;
            case avutil.AV_SAMPLE_FMT_S32P://有符号short 32bit，平面型，但是32bit的话可能电脑声卡不支持，这种音乐也少见
                audioFormat = new AudioFormat(encoding, sampleRate, sampleSizeInBits_32, audioChannels, frameSize, sampleRate, bigEndian);
                break;
            case avutil.AV_SAMPLE_FMT_FLTP://float 平面型 需转为16bit short
                audioFormat = new AudioFormat(encoding, sampleRate, sampleSizeInBits_16, audioChannels, frameSize, sampleRate, bigEndian);
                break;
            case avutil.AV_SAMPLE_FMT_DBLP:
                break;
            case avutil.AV_SAMPLE_FMT_S64://有符号short 64bit 非平面型
                break;
            case avutil.AV_SAMPLE_FMT_S64P://有符号short 64bit平面型
                break;
            default:
                System.out.println("不支持的音乐格式");
        }
        DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, audioFormat, AudioSystem.NOT_SPECIFIED);
        try {
            SourceDataLine sourceDataLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
            sourceDataLine.open(audioFormat);

            return sourceDataLine;
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }

        return null;
    }

    private ByteBuffer shortToByteValue(ShortBuffer arr, float vol) {
        int len = arr.capacity();
        ByteBuffer bb = ByteBuffer.allocate(len * 2);
        for (int i = 0; i < len; i++) {
            bb.putShort(i * 2, (short) ((float) arr.get(i) * vol));
        }
        return bb; // 默认转为大端序
    }

    private ByteBuffer floatToByteValue(FloatBuffer arr, float vol) {
        int len = arr.capacity();
        float f;
        float v;
        ByteBuffer res = ByteBuffer.allocate(len * 2);
        v = 32768.0f * vol;
        for (int i = 0; i < len; i++) {
            f = arr.get(i) * v;//Ref：https://stackoverflow.com/questions/15087668/how-to-convert-pcm-samples-in-byte-array-as-floating-point-numbers-in-the-range
            if (f > v)
                f = v;
            if (f < -v)
                f = v;
            //默认转为大端序
            res.putShort(i * 2, (short) f);//注意乘以2，因为一次写入两个字节。
        }
        return res;
    }

    private void createPlayer() {
        try {
            fmpegFrameGrabber = FFmpegFrameGrabber.createDefault(mediaPath);
            fmpegFrameGrabber.start();
            sourceDataLine = getSourceDataLine(fmpegFrameGrabber);
            if (null != sourceDataLine) {
                sourceDataLine.start();
            }
            sampleFormat = fmpegFrameGrabber.getSampleFormat();
            duration = fmpegFrameGrabber.getLengthInTime();
            lengthInFrames = fmpegFrameGrabber.getLengthInFrames();
            if (null != onMoviePlayingListener) {
                onMoviePlayingListener.onMovieDuration(duration, lengthInFrames);
            }
        } catch (FFmpegFrameGrabber.Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnMoviePlayingListener {
        void onMovieDuration(long duration, int lengthFrames);

        void onMoviePlaying(long timestemp, int frameNumber);

        void onMoviePlayFinish();
    }

}