package com.cuiweiyou.cutor;

import java.io.File;

/**
 * 根据.获取文件后缀
 * www.gaohaiyan.com
 */
public class MediaUtil {

    private MediaUtil() {
    }

    /**
     * 根据.获取文件后缀
     *
     * @param mediaPath
     * @return
     */
    public static String getMediaSuffix(String mediaPath) {
        int index = mediaPath.lastIndexOf(".");
        return mediaPath.substring(index).toLowerCase();
    }

    /**
     * 根据.获取文件名
     *
     * @param mediaPath
     * @return
     */
    public static String getFileName(String mediaPath) {
        int slash;
        String name = SystemUtil.getSystemName();
        if ("windows".equals(name)) {
            slash = mediaPath.lastIndexOf("\\");
        } else {
            slash = mediaPath.lastIndexOf("/");
        }
        int point = mediaPath.lastIndexOf(".");
        return mediaPath.substring(slash + 1, point).toLowerCase();
    }

    /**
     * 视频文件所在的目录
     *
     * @param mediaPath
     * @return 结尾不带斜杠
     */
    public static String getMediaFloder(String mediaPath) {
        File file = new File(mediaPath);
        return file.getParent();
    }

    /**
     * 输出路径
     *
     * @param mediaPath 电影视频源文件全路径
     * @param outFlder  子目录名
     * @return 结尾带斜杠 /
     */
    public static String getCutOutputFloder(String mediaPath, String outFlder) {
        String floder = getMediaFloder(mediaPath);
        String out = floder + "/" + outFlder;
        File outPath = new File(out);
        if (!outPath.exists()) {
            outPath.mkdirs();
        }

        return outPath.getAbsolutePath() + "/";
    }
}
