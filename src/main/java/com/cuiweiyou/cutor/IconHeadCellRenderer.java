package com.cuiweiyou.cutor;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * 列表的item的渲染器
 * www.gaohaiyan.com
 */
public class IconHeadCellRenderer extends JLabel implements ListCellRenderer<ClipsModel> {
    
    // 处理每个item
    @Override
    public Component getListCellRendererComponent(JList list, ClipsModel value, int index, boolean isSelected, boolean cellHasFocus) {

        ClipsModel model = value;
        String startTime = model.getStartTime();
        String stopTime = model.getStopTime();
        String clips = " [" + index + "] " + startTime + "  ——  " + stopTime + "  ——  ";
        boolean selected = model.isSelected();
        if (selected) {
            clips += "✓";
            setForeground(Color.BLUE);
        } else {
            clips += "∅";
            setForeground(Color.BLACK);
        }
        setText(clips);
        setIconTextGap(30);

        return this;
    }
}