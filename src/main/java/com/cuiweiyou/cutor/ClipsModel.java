package com.cuiweiyou.cutor;

/**
 * 视频段model
 * 时间格式 00:00:00
 * www.gaohaiyan.com
 */
public class ClipsModel {
    private String startTime;
    private String stopTime;
    private boolean selected;

    public ClipsModel() {
    }

    /**
     * 片段
     *
     * @param startTime 开始时刻 时间格式 00:00:00
     * @param stopTime  结束时刻 时间格式 00:00:00
     * @param selected  是否默认选中，默认true。选中则进行剪辑
     */
    public ClipsModel(String startTime, String stopTime, boolean selected) {
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.selected = selected;
    }

    /**
     * 时间格式 00:00:00
     * @return
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * 时间格式 00:00:00
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * 时间格式 00:00:00
     * @return
     */
    public String getStopTime() {
        return stopTime;
    }

    /**
     * 时间格式 00:00:00
     */
    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return "片段{" + "开始='" + startTime + '\'' + ", 结束='" + stopTime + '\'' + ", 选中=" + selected + '}';
    }
}