package com.cuiweiyou.cutor;

/**
 * 系统工具
 * www.gaohaiyan.com
 */
public class SystemUtil {

    private static String systemName;

    private SystemUtil() {
    }

    /**
     * 获取系统平台名称：windows、mac、linux
     */
    public static String getSystemName() {
        if (null != systemName) {
            return systemName;
        }
        systemName = System.getProperty("os.name").toLowerCase();
        return systemName;
    }
}
