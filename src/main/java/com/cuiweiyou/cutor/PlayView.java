package com.cuiweiyou.cutor;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.color.ColorSpace;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import javax.swing.JPanel;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;

/**
 * 可嵌入到JFrame或JPanel的FFmpegFrameGrabber载体
 * www.gaohaiyan.com
 */
public class PlayView extends JPanel {
    private PlayCanvas playCanvas;
    private Java2DFrameConverter converter;

    /**
     * 在窗体 setVisible(true); 之前进行定义，并加入父容器
     * 设置宽高、背景、布局、边框 等属性。
     * 如果界面会出现偏差，尝试其它布局
     */
    public PlayView() {
    }

    /**
     * 初始化。须在窗体 setVisible(true); 之后调用
     */
    public void init() {
        playCanvas = new PlayCanvas();
        add(this.playCanvas); // 先 添加画布，再进行其它设置

        playCanvas.setSize(getPreferredSize()); // 画布尺寸
        playCanvas.createBufferStrategy(2); // 2个缓冲区

        converter = new Java2DFrameConverter();
    }

    /**
     * 播放
     * 播放一张图片---FFmpegFrameGrabber读取的视频的某一帧
     *
     * @param frame JavaCV格式的视频帧
     */
    public void playFrame(Frame frame) {
        BufferedImage image = converter.getBufferedImage(frame, 1.0D, false, (ColorSpace) null);
        if (image != null) {
            playCanvas.setPlayImage(image);
            playCanvas.paint(null); // 调用的是重写的方法
        }
    }

    /**
     * 自定义画布
     */
    private class PlayCanvas extends Canvas {
        private Image image;

        public void setPlayImage(Image img) {
            image = img;
        }

        // 重写方法
        @Override
        public void update(Graphics g) {
            this.paint(g);
        }

        // 重写方法
        @Override
        public void paint(Graphics graph) {
            try {
                int width = getWidth();
                int height = getHeight();

                if (width <= 0 || height <= 0) {
                    return;
                }

                if (image != null) {
                    int imgWidth = image.getWidth(null);
                    int imgHeight = image.getHeight(null);

                    double widthRatio = (double) width / imgWidth;
                    double heightRatio = (double) height / imgHeight;

                    double scale = Math.min(widthRatio, heightRatio);

                    int drawWidth = (int) (scale * imgWidth);
                    int drawHeight = (int) (scale * imgHeight);

                    int x = (width - drawWidth) / 2;
                    int y = (height - drawHeight) / 2;

                    BufferStrategy strategy = getBufferStrategy();

                    while (true) {
                        Graphics g = strategy.getDrawGraphics();

                        if (image != null) {
                            g.drawImage(image, x, y, drawWidth, drawHeight, null);
                        }

                        g.dispose();
                        if (!strategy.contentsRestored()) {
                            strategy.show();
                            if (!strategy.contentsLost()) {
                                break;
                            }
                        }
                    }
                }
            } catch (NullPointerException var3) {
            } catch (IllegalStateException var4) {
            }
        }
    }
}
