package com.cuiweiyou.cutor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.CountDownLatch;

/**
 * 异步任务：剪辑分段
 * www.gaohaiyan.com
 */
public class CutTask implements Runnable {
    private CountDownLatch countDownLatch;
    private String shell;

    /**
     * 异步任务：剪辑分段
     *
     * @param latch 计数
     * @param shell
     */
    public CutTask(CountDownLatch latch, String shell) {
        this.countDownLatch = latch;
        this.shell = shell;
    }

    @Override
    public void run() {
        execSync();
        countDownLatch.countDown(); // 数量减1
    }

    // 执行shell
    private String execSync() {
        System.out.println("shell：" + shell);
        System.out.println("shell time：" + System.currentTimeMillis());
        System.out.println("shell thread：" + Thread.currentThread().getName());
        BufferedReader bufferedReader = null;
        String result = "";
        try {
            Process process = Runtime.getRuntime().exec(shell);
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            StringBuilder sb = new StringBuilder();
            while (null != (line = bufferedReader.readLine())) {
                if (!line.isEmpty()) {
                    sb.append(line);
                }
            }
            process.waitFor();
            result = sb.toString();
            System.out.println("shell执行ok：" + result);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("shell执行IOException：" + e.getMessage());
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.err.println("shell执行InterruptedException：" + e.getMessage());
        } finally {
            if (null != bufferedReader) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }
}
